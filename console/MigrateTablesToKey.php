<?php namespace Key\Subscribers\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

/**
 * MigrateTablesToKey Command
 *
 * @link https://docs.octobercms.com/3.x/extend/console-commands.html
 */
class MigrateTablesToKey extends Command
{
    /**
     * @var string signature for the console command.
     */
    protected $signature = 'key:migratesubscribertablestokey';

    /**
     * @var string description is the console command description
     */
    protected $description = 'Rename greymenco tables to Key';

    /**
     * handle executes the console command.
     */
    public function handle()
    {
        $backupDirectory = storage_path('app/backups');
        if (!file_exists($backupDirectory)) {
            mkdir($backupDirectory, 0755, true);
        }

        $backupFileName = 'database_backup_keysubscribers_' . date('Y-m-d_H-i-s') . '.sql';
        $backupPath = $backupDirectory . '/' . $backupFileName;

        $tablesToBackup = [
            'greymen_subscribers_subscribers',
            'greymen_subscribers_campaigns',
            'greymen_subscribers_form',
        ];

        $tablesToRename = [
            'greymen_subscribers_subscribers' => 'key_subscribers_subscribers',
            'greymen_subscribers_campaigns' => 'key_subscribers_campaigns',
            'greymen_subscribers_form' => 'key_subscribers_form',
        ];

        $backupCommand = sprintf(
            'mysqldump -u%s -p%s %s %s > %s',
            env('DB_USERNAME'),
            env('DB_PASSWORD'),
            env('DB_DATABASE'),
            implode(' ', $tablesToBackup),
            $backupPath
        );
        exec($backupCommand);

        foreach ($tablesToRename as $oldTable => $newTable) {
            if (Schema::hasTable($oldTable)) {
                Schema::rename($oldTable, $newTable);
                $this->info("Table '$oldTable' renamed to '$newTable'.");
            } else {
                $this->error("Table '$oldTable' does not exist.");
            }
        }

        $this->info("Backup saved to: $backupPath");
    }
}
