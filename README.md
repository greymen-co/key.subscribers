### Introduction
The Subscriber plugins stores data in the database and can passed them thru to plaforms like MailChimp or Active Campaign. This plugin replaces the old Codestackers version

#### Fields
To use them in blocks:
```
    form_id:
      label: Form ID
      type: dropdown
      emptyOption: -- Select campaign id --
      comment: 'You can modify campaigns <a href="/backend/key/subscribers/campaigns" target="_blank">here</a>'
      commentHtml: true
      required: true
```

```
    list_id:
      label: Active Campaign List ID
      type: dropdown
      options: getListOptions
      emptyOption: -- Select list id --

```

**Branch**
The branch to commit the content to, when the branch doesn't exists it will be created.

**Message**
The message for every commit. In the message you can use the variables %f for the saved file and %u for the  OctoberCMS user that modified the page.
