<?php namespace Key\Subscribers\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Subscribers extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'admin_subscribers'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Key.Subscribers', 'main-menu-item', 'side-menu-subscribers');
    }
}
