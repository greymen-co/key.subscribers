<?php namespace Key\Subscribers\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Forms extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'admin_form'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Key.Subscribers', 'main-menu-item', 'side-menu-form');
    }
}
