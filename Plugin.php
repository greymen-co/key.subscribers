<?php namespace Key\Subscribers;

use System\Classes\PluginBase;
use Key\Subscribers\Classes\ActiveCampaign;
use Key\Subscribers\Models\Campaigns;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Key Subscribers',
            'description' => 'Subscribe email addresses and passes them on to Active Campaign or MailChimp.',
            'author'      => 'Key',
            'icon'        => 'icon-envelope-square',
            'homepage'    => 'https://keyagency.nl/'
        ];
    }

    public function boot()
    {
        \RainLab\Pages\Classes\Page::extend(function($model) {
            $model->addDynamicMethod('getPageOptions', function() {
                $theme = \Cms\Classes\Theme::getEditTheme();
                $pageList = new \RainLab\Pages\Classes\PageList($theme);
                $pages = [];
                foreach ($pageList->listPages() as $name => $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . ' (' . $pageObject->url . ')';
                }
                return $pages;
            });
            $model->addDynamicMethod('getListOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getLists();
            });
            $model->addDynamicMethod('getAcFieldNameOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getFields();
            });
            $model->addDynamicMethod('getFormIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });
            $model->addDynamicMethod('getCampaignIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });

        });
    }
    public function registerComponents()
    {
        return
        [
            'Key\Subscribers\Components\Contact'       => 'Contact',
            'Key\Subscribers\Components\Newsletter'    => 'Newsletter',
            'Key\Subscribers\Components\Form'          => 'Form',
        ];
    }
    public function registerMailTemplates()
    {
        return [
            'key.subscribers::mail.subscribed',
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'key.subscribers::lang.plugin.name',
                'category' => 'system::lang.system.categories.cms',
                'icon' => 'oc-icon-user',
                'description' => 'key.subscribers::lang.plugin.description',
                'class' => 'Key\Subscribers\Models\Settings',
                'order' => 800,
            ]
        ];
    }
    public function registerReportWidgets()
    {
        return [
            'Key\Subscribers\ReportWidgets\SubscriberStats' => [
                'label'   => 'Subscribers stats',
                'context' => 'dashboard'
            ]
        ];
    }

    public function register()
    {
        $this->registerConsoleCommand('key.migratesubscribertablestokey', \Key\Subscribers\Console\MigrateTablesToKey::class);
    }


}
