<?php namespace Key\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateKeySubscribersCampaigns extends Migration
{
    public function up()
    {
        Schema::create('key_subscribers_campaigns', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->text('description')->nullable();
            $table->string('utm', 150)->nullable();
            $table->string('ac_tag', 100)->nullable();
            $table->integer('ac_list_id')->nullable();
            $table->boolean('send_data_to_ac')->nullable();
            $table->boolean('send_data_to_url')->nullable();
            $table->boolean('send_data_to_mail')->nullable();
            $table->text('send_data_url')->nullable();
            $table->text('send_data_mail_subject')->nullable();
            $table->text('send_data_mail_from')->nullable();
            $table->text('send_data_mail_to')->nullable();
            $table->text('form')->nullable();
            $table->string('send_data_zapier_url', 200)->nullable();
            $table->boolean('send_data_to_zapier')->nullable();
            $table->string('comments_icon', 150)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->index('name', 'idx_name');
            $table->index('ac_list_id', 'idx_ac_list_id');
            $table->index('created_at', 'idx_created_at');
        });

        Schema::create('key_subscribers_subscribers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('campaign_id')->nullable();
            $table->string('language_code', 2)->nullable();
            $table->string('fullname', 150)->nullable();
            $table->string('firstname', 100)->nullable();
            $table->string('lastname', 100)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('gender', 1)->nullable();
            $table->string('zipcode',150)->nullable();
            $table->string('address',150)->nullable();
            $table->string('city',100)->nullable();
            $table->boolean('winner')->nullable();
            $table->string('subject', 255)->nullable();
            $table->text('message')->nullable();
            $table->text('browser')->nullable();
            $table->string('ip',255)->nullable();
            $table->string('utm',255)->nullable();
            $table->boolean('newsletter')->nullable();
            $table->boolean('privacy')->nullable();
            $table->text('form_data')->nullable();
            $table->timestamp('synced_with_ac_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->index('campaign_id','idx_campaign_id');
            $table->index('language_code','idx_language_code');
            $table->string('company_name', 150)->nullable();
            $table->index('email','idx_email');
            $table->index('created_at','idx_created_at');
        });

        Schema::create('key_subscribers_form', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('shortcode', 15);
            $table->string('name', 50)->nullable();
            $table->text('html')->nullable();
            $table->string('file', 150);
        });

    }

    public function down()
    {
        Schema::dropIfExists('key_subscribers_campaigns');
        Schema::dropIfExists('key_subscribers_subscribers');
        Schema::dropIfExists('key_subscribers_form');
    }
}
