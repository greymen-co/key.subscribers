<?php
namespace Key\Subscribers\Models;

use Model;

/**
 * Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'key_subscribers_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
