<?php

namespace Key\Subscribers\Components;

use Cms\Classes\ComponentBase;

use Session;
use Validator;
use Input;
use Request;
use AjaxException;
use ValidationException;
use Flash;
use Lang;
use Http;
use Redirect;

use Key\Subscribers\Classes\ActiveCampaign;
use Key\Subscribers\Models\Settings;

use Key\Subscribers\Models\Subscribers;
use Key\Subscribers\Models\Preview;

class Newsletter extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact to newsletter',
            'description' => 'Subscribe contact to newsletter  '
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    protected function process_active_campaign($data)
    {
        $active_campaign = new ActiveCampaign(
            Settings::instance()->ac_url,
            Settings::instance()->ac_api_key
        );
        //        dump(explode(',', $data['tags']));
        if (!isset($data['list_id'])) return false;
        setlocale(LC_ALL, 'nl_NL');
        $ac     = [
            'last_name'                                    => $data['fullname'],
            'email'                                        => $data['email'],
            "p[" . $data['list_id'] . "]"                  => $data['list_id'],
            "tags"                                         => $data['tags'],
        ];

        return $active_campaign->call('contact/sync', $ac);
    }
    protected function process_post_url($data)
    {
        $postData = false;

        if (! isset($data['post_url'])) return false;
        $url = $data['post_url'];
        unset($data['post_url']);
        unset($data['form_data']);
        $data['MERGE0'] = $data['email'];
        $data['MERGE1'] = $data['fullname'];


        if(isset($data['post_data']))
        {
            $form = '<form id="postForm" action="'.$url.'" method="post">';
            foreach ($data as $k => $v) {
                $form.='<input type="hidden" name="'.htmlentities($k).'" value="'.htmlentities($v).'">';
            }
            $form .= '</form>
            <script type="text/javascript">
                document.getElementById("postForm").submit();
            </script>';
            return $form;
        } else {
            $query_url = strpos($url,'?') > 0 ? $url.'&'.http_build_query($data) : $url.'?'.http_build_query($data);
            return Redirect::to($query_url);
        }
    }

    public function onSubmit()
    {
        $data                       = post();

        $rules = [
            'fullname'              => 'required',
            'email'                 => 'required|email'
        ];
        $validation = Validator::make($data, $rules,  Lang::get('key.subscribers::validation'));
        if ($validation->fails()) {
            throw new ValidationException($validation);
        };

        $subscriber                 = new Subscribers();
        $data['form_data']          = json_encode($data);
        $subscriber->fill($data);
        $subscriber->save();

        $this->process_active_campaign($data);

        $result = $this->process_post_url($data);
        if ($result)
        {
            if(isset($data['post_data']))
            {
                return [
                    '#' . post('form_id')    => $result,
                    'success' => true
                ];

            } else {
                return $result;
            }
        } else {
            return [
                '#' . post('form_id')    => post('thank_you_message'),
                'success' => true
            ];
        }

    }


    public function onRun()
    {
        // $this->page['yoga']  = true;
    }
}
