<?php namespace Key\Subscribers\Components;

use Cms\Classes\ComponentBase;
use Key\Subscribers\Models\Campaigns;
use Key\Subscribers\Models\Subscribers;
use Key\Subscribers\Classes\ActiveCampaign;

use Http;
use Session;
use Validator;
use Input;
use Redirect;
use AjaxException;
use ValidationException;
use Flash;
use Lang;
use Mail;
use Event;
use Log;

class Form extends ComponentBase
{

    var $campaign;
    var $data;
    public function componentDetails()
    {
        return [
            'name'        => 'Form Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'campaign_id' => [
                'title'             => 'Campaign ID',
                'description'       => 'Id of the campaign ',
                'type'              => 'number'
            ],
            'form_id' => [
                'title'             => 'Form ID',
                'description'       => 'ID of the element to send the result to',
                'type'              => 'text'
            ],
            'hide_labels' => [
                'title'             => 'Hide form labels',
                'description'       => 'Hides the labels of a input field',
                'type'              => 'switch'
            ],
            'thankyou_title' => [
                'title'             => 'Form Thank You title',
                'description'       => 'Titel na insturen formulier',
                'type'              => 'text'
            ],
            'thankyou_text' => [
                'title'             => 'Form Thank You text',
                'description'       => 'Tekst onder title na insturen formulier',
                'type'              => 'text'
            ],

        ];
    }
    public function onRender()
    {
        $this->page['campaign_id']      = $this->property('campaign_id');
        $this->page['form_id']          = $this->property('form_id');
        $this->page['thankyou_title']   = $this->property('thankyou_title');
        $this->page['thankyou_text']    = $this->property('thankyou_text');
        $this->page['hide_labels']      = $this->property('hide_labels');
        $this->page['campaign']         = Campaigns::getCampaignById($this->property('campaign_id'), $this);
        //        dump($this->page['campaign']);

    }
    public function onRun()
    {
        $this->addJs("/plugins/key/subscribers/assets/js/validation.js", "1.0.0");
        $this->addCss("/plugins/key/subscribers/assets/css/validation.css", "1.0.0");
    }
    public function errorMessages()
    {
        $messages   = Lang::get('key.subscribers::validation');
        $elements   = $this->campaign['data']['form'];
        foreach($elements as $element)
        {
            $rules = explode('|',$element['validation_rules']);
            foreach($rules as $rule)
            {
                $rule = $element['name'] . '.' .explode(':',$rule)[0];
                $messages[$rule] = $element['validation_error_message'];
            }
        }
        return $messages;
    }

    public function onSubmit()
    {
        $this->campaign  = Campaigns::getCampaignById(post('campaign_id'), $this);
        if(!$this->campaign)
        {
            throw new ValidationException('Campaign not found');
        }
        $data   = post();
        $rules  = json_decode(post('rules'),true);
//        $validation = Validator::make($data, $rules,  Lang::get('key.subscribers::validation'));
        $validation = Validator::make($data, $rules, $this->errorMessages($data) );

        if ($validation->fails()) {
            throw new ValidationException($validation);
        };

        $subscriber                 = new Subscribers();
        $data['form_data']          = json_encode($data);
        $subscriber->fill($data);
        $subscriber->save();
        unset($data['form_data']);
        unset($data['rules']);
        unset($data['thankyou_title']);
        unset($data['thankyou_text']);
        unset($data['thankyou_text']);

        Event::fire('subscriber.submit', [$data]);

        if (intval($this->campaign['data']['send_data_to_zapier'])===1) $this->sendDataToZapier($data);
        if (intval($this->campaign['data']['send_data_to_mail'])===1) $this->sendConfirmationMail($data);
        if (intval($this->campaign['data']['send_data_to_ac'])===1) $this->sendDataToAc($data);
        return [
            '#' . post('form_id')   => '<h5 >'.post('thankyou_title').'</h5><p>'.post('thankyou_text').'</p>',
            '#modal-btn--send'      => "",
            '.remove-after-send'    => "",
            'success'               => true
        ];
    }

    private function sendDataToZapier($data)
    {
        $url = $this->campaign['data']['send_data_zapier_url'];
//        $url = 'https://hooks.zapier.com/hooks/catch/4406254/bb9ljhb';
        $this->data = $data;
        $result =  Http::post($url , function($http){
            $http->requestData = $this->data;
            $http->setOption(CURLOPT_SSL_VERIFYHOST, false);
        });
        if($result->code ===200) {
            Log::info('Zapier data send result: '.$result->body);
            return true;
        } else{
            Log::error('Error sending data to Zapier: '.$result->body);
            return false;
        }
    }

    private function sendDataToAc($data)
    {
        $active_campaign = new ActiveCampaign();

        setlocale(LC_ALL, 'nl_NL');
        $c_data = $this->campaign['data'];
        $ac     = [
            // 'email'                             => post('email'),
            "p[" . $c_data['ac_list_id']. "]"   => $c_data['ac_list_id'],
            "tags"                              => $c_data['ac_tag'],
        ];


        foreach($this->campaign['fields'] as $field)
        {
            switch ($field['ac_field_name']) {
                case 'EMAIL':
                    $ac['email'] = post($field['name']);
                    break;
                case 'FIRSTNAME':
                    $ac['first_name'] = post($field['name']);
                    break;
                case 'LASTNAME':
                    $ac['last_name'] = post($field['name']);
                    break;
                case 'PHONE':
                    $ac['phone'] = post($field['name']);
                    break;
                default:
                    if($field['ac_field_name'])
                    {
                        $ac_field = "field[%".$field['ac_field_name']."%]";
                        $ac[$ac_field] = post($field['name']);
                    }
                    break;
                }
        }

        /* ADD HIDDEN % FIELDS TO AC */
        foreach($data as $key => $value)
        {
            if( substr($key ,0,1)==="%")
                $ac["field[$key]"] = $value;
        }
        // dd($ac);
        $response = $active_campaign->call('contact/sync', $ac);
        return $response;
    }

    private function sendConfirmationMail($data)
    {
        $vars['data'] = $data;

        Mail::send('key.subscribers::mail.subscribed', $vars, function($message) {
            $message->from('info@keyagency.nl', 'website');
            $message->to($this->campaign['data']['send_data_mail_to']);
            $message->subject($this->campaign['data']['send_data_mail_subject']);
        });
    }

}
