<?php namespace Key\Subscribers\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Artisan;
use Flash;
use Lang;
use Db;
use Key\Subscribers\Models\Subscribers;
class SubscriberStats extends ReportWidgetBase
{
    protected $defaultAlias = 'subscribers_subscribers';

    public function render(){
        $this->vars['data']     = $this->getSubscribers();
        $this->vars['radius']   = $this->property("radius");
        $widget = 'widget';
        return $this->makePartial($widget);
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'Subscriber stats',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ],
            'radius' => [
                'title'             => 'key.subscribers::lang.plugin.radius',
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Only numbers!',
                'default'           => '200',
            ],
        ];
    }

    private function getSubscribers(){
        $data['subscribers']    = Subscribers::select(Db::raw('count(*) as count, key_subscribers_campaigns.name as name'))
                                    ->join('key_subscribers_campaigns','campaign_id','=','key_subscribers_campaigns.id')
                                    ->groupBy('key_subscribers_campaigns.name')
                                    ->get()
                                    ->toArray();
        $data['count']          = Subscribers::count();
        return $data;
    }

}
