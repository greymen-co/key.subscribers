<?php
namespace Key\Subscribers\Classes;

class ApiException extends \Exception
{

}

use Key\Subscribers\Models\Settings;

class ActiveCampaign
{
    public $active_campaign = false;
    private $config;

    public function __construct($ac_url=null, $ac_key=null)
    {
        $ac_url = Settings::instance()->ac_url;
        $ac_key = Settings::instance()->ac_api_key;
        if (is_null($ac_url) || is_null($ac_key)) return false;
        $this->active_campaign = new \ActiveCampaign($ac_url, $ac_key);
    }

    public function call($url, $data)
    {
        $copy = [];
        if (! $this->active_campaign) return false;
        foreach ($data as $key => $val) {
            $copy[urlencode($key)] = $val instanceof \DateTime ? $val->format('Y/m/d') : $val;
        }
        $response = $this->active_campaign->api($url, $copy);
        if (is_string($response)) {
            throw new ApiException($response);
        } else if ($response->success === 0) {
            throw new ApiException($response->result_message);
        }

        return $response;
    }

    public function getLists()
    {
        if (! $this->active_campaign) return [-1=>"please check you Active Campaign API key"];

        $params = [
            'ids' => 'all',
        ];
        $lists = $this->call('list/list', $params);
        $return = [];
        foreach($lists as $list)
        {
            if (isset($list->id))
            {
                $return[$list->id]=$list->name;
            }
        }
        return $return;
    }

    public function getFields()
    {
        if (! $this->active_campaign) return [-1=>"please check you Active Campaign API key"];
        $fields = $this->active_campaign->api('list/field/view?ids=all');

        $return = [];
        $return['FIRSTNAME']= '%FIRSTNAME%';
        $return['LASTNAME']= '%LASTNAME%';
        $return['PHONE']= '%PHONE%';
        $return['EMAIL']= '%EMAIL%';
        foreach($fields as $field)
        {
            if (isset($field->tag))
            {
                $tag = str_replace('%','',$field->tag);
                $return[$tag]=$field->tag;
            }
        }
        return $return;
    }


}
