<?php namespace Key\Subscribers\Classes;

use Key\Subscribers\Models\Campaigns;
use Key\Subscribers\Models\Subscribers;
use Key\Subscribers\Classes\ActiveCampaign;
use Backend\Classes\Controller;

use Http;
use Session;
use Validator;
use Input;
use Redirect;
use AjaxException;
use ValidationException;
use Flash;
use Lang;
use Mail;

class SyncBySession extends Controller
{

    var $campaign;
    var $fields = [];
    public function onSubmit($campaign_id=0)
    {
        $this->campaign  = Campaigns::find($campaign_id);

        if(!$this->campaign)
        {
            throw new ValidationException('Campaign not found');
        }
        $data                       = Session::all();
        $data['campaign_id']        = $campaign_id;

        $subscriber                 = new Subscribers();
        $data['form_data']          = json_encode($data);
        $subscriber->fill($data);
        $subscriber->save();
        $data['ac_list_id'] = $this->campaign['ac_list_id'];
        $data['ac_tag'] = $this->campaign['ac_tag'];
        unset($data['form_data']);
        unset($data['rules']);
        unset($data['thankyou_title']);
        unset($data['thankyou_text']);
        unset($data['thankyou_text']);
        if (intval($this->campaign['send_data_to_mail'])===1) $this->sendConfirmationMail($data);
        if (intval($this->campaign['send_data_to_ac'])===1) $this->sendDataToAc($data);
        // return [
        //     '#' . post('form_id')   => '<div class="thankyou"><h5 >'.post('thankyou_title').'</h5><p>'.post('thankyou_text').'</p></div>',
        //     '#modal-btn--send'      => "",
        //     '.remove-after-send'    => "",
        //     'success'               => true
        // ];
    }

    private function sendDataToAc($data)
    {
        $active_campaign = new ActiveCampaign();
        setlocale(LC_ALL, 'nl_NL');
        $ac     = [
            // 'email'                             => post('email'),
            "p[" . $this->campaign['ac_list_id']. "]"   => $this->campaign['ac_list_id'],
            "tags"                                      => $this->campaign['ac_tag'],
            "email"                                     => isset($data['email']) ? $data['email']:'',
            "first_name"                                => isset($data['firstname']) ? $data['firstname']:'',
            "last_name"                                 => isset($data['lastname']) ? $data['lastname']:'',
            "field[%STATIONTYPE%]"                      => isset($data['stationtype']) ? $data['stationtype']:'',
            "field[%NETBEHEERDER%]"                     => isset($data['netbeheerder']) ? $data['netbeheerder']:'',
            "field[%TRANSFORMATORVERMOGEN%]"            => isset($data['transformatorvermogen']) ? $data['transformatorvermogen']:'',
            "field[%INCLUDERENTAL%]"                    => isset($data['includerental']) ? $data['includerental']:'',
            "field[%INCLUDELAAGSPANNINGSVERDELING%]"    => isset($data['includelaagspanningsverdeling']) ? $data['includelaagspanningsverdeling']:'',
            "field[%FIELDSSETUP%]"                      => isset($data['fieldssetup']) ? $data['fieldssetup']:'',
            "field[%UITVOERINGDBT%]"                    => isset($data['uitvoeringdbt']) ? $data['uitvoeringdbt']:'',
            "field[%INCLUDEMIDDENSPANNINGSCHAKELAAR%]"  => isset($data['includemiddenspanningschakelaar']) ? $data['includemiddenspanningschakelaar']:'',
            "field[%PRICE%]"                            => isset($data['price']) ? $data['price']:'0',
            "field[%PRODUCT%]"                          => isset($data['product']) ? $data['product']:'Geen indicatie mogelijk',
        ];

        // dd($ac);
        $response = $active_campaign->call('contact/sync', $ac);
        // dd($response);
        return $response;
    }

    private function sendConfirmationMail($data)
    {
        $vars = $data;
        if (isset($vars['rainlab'])) unset($vars['rainlab']);
        if (isset($vars['_flash'])) unset($vars['_flash']);
        if (isset($vars['widget'])) unset($vars['widget']);
        if (isset($vars['_previous'])) unset($vars['_previous']);

        $vars['data'] = $vars;

        Mail::send('key.subscribers::mail.subscribed', $vars, function($message) {
            $message->from('info@keyagency.nl', 'website');

            $message->to($this->campaign['send_data_mail_to']);
            $message->subject($this->campaign['send_data_mail_subject']);
        });
    }

}
