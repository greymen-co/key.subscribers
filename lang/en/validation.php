<?php
return
    [
        'fullname.required' => 'Je bent vergeten je naam in te vullen',
        'email.required' => 'Je bent vergeten je email in te vullen',
        'email.email' => 'Je bent vergeten je email in te vullen',
    ];
